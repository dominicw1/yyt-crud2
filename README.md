## Documentation

```
const express = require('express')
const mysql = require('mysql')
```
Import dependencies (ExpressJS & mySQL).

```const port = 3000```
Use port 3000 to serve pages.

```
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'tasks-database'
})


connection.connect()
```
Setup and connect to mySQL database. ```host```, ```user```, ```password``` and ```database```
should be set to match the credentials of the desired database.

```
app.get('/', (req, res) => {
    res.send("Task Manager")
})
```
Renders a simple sentence on the home page of the app.

```
app.get('/read-tasks', (req, res) => {
    const sql = 'SELECT name, due_date, completion_date, status FROM tasks'
    connection.query(sql, function(error, results) {
        if (error) throw error
    
        res.send(results)
    })
})
```
Queries the database for the name, due date, completion date and status of the tasks.
I have assumed that the ID is used for internal database purposes and would not be shown to the user.
I would anticipate that in production it would be necessary to process the response rather than just render it as is.

```
app.post('/save-task', (req, res) => {
    const data = {name: req.body.name, due_date: req.body.due, completion_date: req.body.completion, status: req.body.status}
    const sql = `INSERT INTO tasks (name, due_date, completion_date, status) VALUES (${data.name}, ${data.due_date}, ${data.completion_date}, ${data.status})`
    connection.query(sql, function(error, results) {
        if (error) throw error

        res.redirect('/')
    })
})
```
Queries the database to save a new task in the database. This uses a POST request because it is attempting to alter the database.
I have assumed that the database will auto-generate the ID when a new record is created.
To make this a functioning app I would expect to connect this to an HTML form where the user would enter the data to be stored in the database.

```
app.post('/update-task:id', (req, res) => {
    const data = {name: req.body.name, due_date: req.body.due, completion_date: req.body.completion, status: req.body.status}
    const sql = `UPDATE tasks (name, due_date, completion_date, status) SET (${data.name}, ${data.due_date}, ${data.completion_date}, ${data.status}) WHERE id=${req.params.id}`
    connection.query(sql, function(error, results) {
        if (error) throw error

        res.redirect('/')
    }) 
})
```
Queries the database to update an existing task. Again, a POST request is used because it is attempting to alter the database.
Again, to make this a functioning app I would expect to connect this to an HTML form where the user would enter the updated data for the task.
The ID is used with a WHERE query to specify the record to be edited.

```
app.post('/delete-task:id', (req, res) => {
    const sql = `DELETE FROM tasks WHERE id=${req.params.id}`
    connection.query(sql, function(error, results) {
        if (error) throw error

        res.redirect('/')
    })
})
```
Queries the database to delete an existing task. Again, a POST request is used because it is attempting to alter the database.
To make this a functioning app I would expect to connect this to an HTML button.
Again, the ID is used with a WHERE query to specify the record to be deleted.

```app.listen(port)```
Setup the port.


## Further considerations:

The ```.gitignore``` file ensures that the node modules files are not tracked by git and uploaded to remote repositories.

I am aware that there would be security considerations to consider before this code could be used in production, such as protection against SQL injection attacks.


## Possible SQL commands to create a suitable database to store the data:

```CREATE DATABASE tasks-database;```
```CREATE TABLE tasks (
    ID int,
    name varchar(100),
    due_date varchar(20),
    completion_date varchar(20),
    status varchar(100),
    PRIMARY KEY (ID)
)
```


## Installation instructions:

clone this repository to your local machine.

run ```npm install``` to install dependencies.
run the app using ```node app.js```
