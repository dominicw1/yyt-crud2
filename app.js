const express = require('express')
const mysql = require('mysql')
const app = express()
const port = 3000

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'tasks-database'
})


connection.connect()




app.get('/', (req, res) => {
    res.send("Task Manager")
})


// Read records
app.get('/read-tasks', (req, res) => {
    const sql = 'SELECT name, due_date, completion_date, status FROM tasks'
    connection.query(sql, function(error, results) {
        if (error) throw error
    
        res.send(results)
    })
})

// Create record
app.post('/save-task', (req, res) => {
    const data = {name: req.body.name, due_date: req.body.due, completion_date: req.body.completion, status: req.body.status}
    const sql = `INSERT INTO tasks (name, due_date, completion_date, status) VALUES (${data.name}, ${data.due_date}, ${data.completion_date}, ${data.status})`
    connection.query(sql, function(error, results) {
        if (error) throw error

        res.redirect('/')
    })
})

// Update record with id
app.post('/update-task:id', (req, res) => {
    const data = {name: req.body.name, due_date: req.body.due, completion_date: req.body.completion, status: req.body.status}
    const sql = `UPDATE tasks (name, due_date, completion_date, status) SET (${data.name}, ${data.due_date}, ${data.completion_date}, ${data.status}) WHERE id=${req.params.id}`
    connection.query(sql, function(error, results) {
        if (error) throw error

        res.redirect('/')
    }) 
})

// Delete record with id
app.post('/delete-task:id', (req, res) => {
    const sql = `DELETE FROM tasks WHERE id=${req.params.id}`
    connection.query(sql, function(error, results) {
        if (error) throw error

        res.redirect('/')
    })
})

app.listen(port)
